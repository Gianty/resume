using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RobotTask
{
    public delegate void Operation(string commLine, Robot robot);

    public class RobotMemory
    {
        private RobotStack<string> stack;

        public RobotMemory()
        {
            stack = new RobotStack<string>();
        }

        public void Push(string value) => stack.Push(value);

        public string Pop() => stack.Pop();

        public string Peek() => stack.Peek();

        public void Read(IEnumerator<string> input)
        {
            input?.MoveNext();
            var result = input == null ? Console.ReadLine() : input.Current;
            Push(result);
        }

        public void Write(List<string> output, bool isConsoleOutput)
        {
            string peek = Peek();
            if (isConsoleOutput)
                Console.WriteLine(peek);
            else
                output.Add(peek);

        }

        public void Swap(int firstIndex, int secondIndex)
        {
            string temp = stack[firstIndex];
            stack[firstIndex] = stack[secondIndex];
            stack[secondIndex] = temp;
        }

        public void Concat()
        {
            string firstElement = Pop();
            string secondElement = Pop();
            Push(firstElement + secondElement);
        }

        public void Copy(int index)
        {
            string target = stack[index];
            Push(target);
        }

        public void ReplaceOne(Robot robot)
        {
            Stopwatch sw = new Stopwatch();

            string targetString = Pop();
            string substringToReplace = Pop();
            string newSubstring = Pop();
            string label = Pop();
            int index = targetString.IndexOf(substringToReplace, StringComparison.Ordinal);

            if (index >= 0)
            {
                targetString =
                    targetString.Substring(0, index + substringToReplace.Length)
                    .Replace(substringToReplace, newSubstring) +
                    targetString.Substring(index + substringToReplace.Length);
            }
            else
            {
                robot.Commands["label"](label, robot);
            }
            Push(targetString);
        }
        

        public int GetCount() => stack.Count;
    }

    public class Robot : IRobot
    {
        private RobotMemory stack;
        private int CommandIndex { get; set; }
        private Dictionary<int, Command> parsedCommands;
        private Dictionary<string, int> labels;
        private List<string> output;
        private bool isConsoleOutput;
        private IEnumerator<string> input;
        private List<string> commands;
        public Dictionary<string, Operation> Commands;

        public List<string> Evaluate(List<string> commands, IEnumerable<string> input)
        {
            Initialize(commands, input);
            int commandsCount = commands.Count;
            CommandIndex = 0;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (CommandIndex < commandsCount)
            {
                string command = commands[CommandIndex];
                bool isCommandEmpty = command.Equals(string.Empty);
                if (!isCommandEmpty)
                {
                    Command parsedCommand = GetCommand(command);
                    ExecuteCommand(parsedCommand, stack, commandsCount);
                }
                CommandIndex++;
            }
            return output;
        }

        public void Evaluate(List<string> commands)
        {
            Evaluate(commands, null);
        }

        private void Initialize(List<string> commandsList, IEnumerable<string> inputLines)
        {
            stack = new RobotMemory();
            labels = new Dictionary<string, int>();
            output = new List<string>();
            parsedCommands = new Dictionary<int, Command>();
            input = inputLines?.GetEnumerator();
            commands = commandsList;
            isConsoleOutput = input == null;

            Commands = new Dictionary<string, Operation>()
            {
                {"push",        (x,y) => y.stack.Push(x.Substring(1, x.Length - 2).Replace("'", "''"))},
                {"pop",         (x,y) => y.stack.Pop()},
                {"write",       (x,y) => y.stack.Write(y.output, y.isConsoleOutput)},
                {"swap",        (x,y) =>
                {
                    var array = x.Split(' ').Select(int.Parse).ToArray();
                    y.stack.Swap(array[0], array[1]);
                }
                },
                {"read",        (x,y) => y.stack.Read(y.input)},
                {"copy",        (x,y) => y.stack.Copy(int.Parse(x))},
                {"concat",      (x,y) => y.stack.Concat()},
                {"label",       (x,y) => y.FindLabel(x) },
                {"jmp",         (x,y) => y.Commands["label"](x,y)},
                {"replaceone",  (x,y) => y.stack.ReplaceOne(y)}
            };
        }

        private void FindLabel(string label)
        {
            if (label.Equals(""))
                FindLabel(stack.Pop());
            else
            {
                int i = 0;
                if (labels.ContainsKey(label)) 
                    i = labels[label];
                else
                {
                    string command = "label " + label;
                    i = commands.FindIndex(x => x.Equals(command, StringComparison.OrdinalIgnoreCase));
                    labels.Add(label, i);

                }
                CommandIndex = i;
            }
        }

        private Command GetCommand(string command)
        {

            if (parsedCommands.ContainsKey(CommandIndex))
                return parsedCommands[CommandIndex];
            else
                return ParseCommand(command);
        }

        private Command ParseCommand(string command)
        {
            string commandName = string.Empty;
            string commandLine;
            Char[] charArray = command.ToArray();
            int j = 0;

            while (j < charArray.Length && charArray[j] != ' ')
                commandName = commandName + charArray[j++];

            commandName = commandName.ToLower();

            if (j != charArray.Length)
                commandLine = command.Substring(j + 1);
            else
                commandLine = "";

            Command parsedCommand = new Command(commandName, commandLine);
            parsedCommands.Add(CommandIndex, parsedCommand);
            return parsedCommand;
        }

        private void ExecuteCommand(Command currentCommand, RobotMemory robotMemory, int commandsCount)
        {
            string commandName = currentCommand.Name;
            string commandLine = currentCommand.Line;
            string command = commandName + commandLine;

            try
            {
                Commands[commandName](commandLine, this);
            }
            catch (Exception ex)
            {
                string message = GetMessage(ex);
                ShowExceptionMessage(message, command, CommandIndex, robotMemory, ex);
                CommandIndex = commandsCount;
            }
        }

        private string GetMessage(Exception ex)
        {
            string message;
            if (ex is ArgumentOutOfRangeException || ex is IndexOutOfRangeException)
            {
                message = "Индекс за пределами стека";
            }
            else if (ex is InvalidOperationException)
                message =
                    "Невозможно совершить операцию удаления из стека, так как стек пуст";
            else if (ex is FormatException)
                message = "Неверный формат входной строки";
            else
                message = ex.GetType().Name;

            return message;
        }

        private void ShowExceptionMessage(string message, string command,
            int i, RobotMemory robotMemory, Exception ex)
        {
            string message2 = string.Format(
                "Последняя операция: {0}, номер операции: {1}", command, i + 1);

            List<string> messages = new List<string>() { message, message2, ex.StackTrace };

            foreach (var m in messages)
            {
                Commands["push"](String.Format("'{0}'", m), this);
                Commands["write"](m, this);
            }
        }

    }

    internal class RobotStack<T>
    {
        private readonly List<T> list = new List<T>();

        public int Count => list.Count;
        private int GetLast => Count - 1;

        public void Push(T value) => list.Add(value);

        public T Pop()
        {
            var pop = list[GetLast];
            list.RemoveAt(GetLast);
            return pop;
        }
        

        public T Peek() => list[GetLast];

        public T this[int index]
        {
            get
            {
                if (index < 1 || index > Count)
                    throw new IndexOutOfRangeException();

                return list[Count - index];
            }
            set
            {
                if (index < 1 || index > Count)
                    throw new IndexOutOfRangeException();

                list[Count - index] = value;
            }
        }
    }

    public class Command
    {
        public string Name { get; }
        public string Line { get; }

        public Command(string name, string line)
        {
            Name = name;
            Line = line;
        }
    }
}